#!/bin/bash
#

# prevent read from removing leading and trailing white spaces
IFS=

# wait for some input (newline terminated)
read line

# wait $1 seconds before replying
sleep "$1"

# echoes input followed by digit 1 and the prompt >
echo "${line}1>"
