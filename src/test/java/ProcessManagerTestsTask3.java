/*
 * This file is part of MQ-ExpectJava.
 *
 * Copyright (C) 2015-2016 Franck Cassez.
 *
 * MQ-ExpectJava is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-ExpectJava is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExpectJava. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Test suite for Assignment 1 Task 3
 */
public class ProcessManagerTestsTask3 {

    /**
     * Test for collectWithTimout
     */
    @Test
    public void Test1() throws Exception  {

        System.out.println("Testing spawnAndCollectWithTimeout() -- process emits result after 2 sec before timeout of 3 sec");
        //  program to spawn and arguments
        String prog = "/Users/franck/development/COMP255-2016/assignment-1-solution/sleepAndEcho.sh";
        String[] args = { "2" };

        //  create a process manager to interact with `ps`

        try {
            ProcessManager p = new ProcessManager(prog, args);
            //  spawn and collect response
            String res = p.spawnAndCollectWithTimeout(3).trim();
            p.destroy();
            //  should get DONE
            assertEquals("DONE", res);
            System.out.println("Test 1 passed");
        }
        catch (Exception e) {
            System.out.println("Test 1 failed");
            throw(e);
        }
    }

    /**
     * Test for spawning a process that outputs a string which is the first arguments passed to it
     */
    @Test(expected=Exception.class)
    public void Test2() throws Exception  {

        System.out.println("Testing spawnAndCollectWithTimeout() -- process emits result after 3 sec after timeout of 2 sec");

        //  program to spawn and arguments
        String prog = "/Users/franck/development/COMP255-2016/assignment-1-solution/sleepAndEcho.sh";
        String[] args = { "3" };

        //  create a process manager to interact with `ps`

        try {
            ProcessManager p = new ProcessManager(prog, args);
            //  spawn and collect response
            String res = p.spawnAndCollectWithTimeout(2).trim();
            p.destroy();
            System.out.println("Test 2 failed");
        }
        catch (Exception e) {
            System.out.println("Test 2 passed (exception)");
            throw(e);
        }
    }
}
