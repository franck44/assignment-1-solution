/*
 * This file is part of MQ-ExpectJava.
 *
 * Copyright (C) 2015-2016 Franck Cassez.
 *
 * MQ-ExpectJava is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-ExpectJava is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExpectJava. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */
 
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

/**
 * Test suite for Assignment 2 Task 4
 */
public class ProcessManagerTestsTask4 {

    /**
     * Testing send(s) but process not spawned -- should raise exception
     */
     @Test(expected=Exception.class)
     public void Test1() throws Exception  {

         System.out.println("Testing send(s) but process not spawned -- should raise exception");
         //  program to spawn and arguments
         String prog = "/Users/franck/development/COMP255-2016/assignment-1-solution/bounceInput.sh";
         String[] args = { "0" };

         //  create a process manager to interact with `prog`

         try {
             ProcessManager p = new ProcessManager(prog, args);
             //  spawn and collect response
             boolean res = p.send("From test task 4");
             assertEquals(true, res);
             p.destroy();
             System.out.println("Test 1 failed");
         }
         catch (Exception e) {
             System.out.println("Test 1 passed (exception)");
             throw(e);
         }
     }

     /**
      * Testing send(s), s does not have whitespaces  -- and expect s followed by >
      */
      @Test
      public void Test2() throws Exception  {

          System.out.println("Testing send(s), s does not have whitespaces  -- prompt is > -- expect should return s with a 1 at the end");
          //  program to spawn and arguments
          String prog = "/Users/franck/development/COMP255-2016/assignment-1-solution/bounceInput.sh";
          String[] args = { "1" };

          //  create a process manager to interact with `prog`

          try {
              ProcessManager p = new ProcessManager(prog, args);
              //  spawn and collect response
              p.spawn();
              //    create the prompt
              Pattern prompt = Pattern.compile(">");

              //    send text
              boolean resSend = p.send("packet\n");
              assertEquals(true, resSend);

              //    expect answer in 1sec
              String res = p.expect(prompt , 1);
              assertEquals("packet1", res);
              System.out.println("Test 2 passed");
          }
          catch (Exception e) {
              System.out.println("Test 2 failed");
              throw(e);
          }
      }

      /**
       * Testing send(s), but expect deadlien is before process echoes result
       */
       @Test(expected=Exception.class)
       public void Test3() throws Exception  {

           System.out.println("Testing send(s), but expect deadline is before the process echoes result");
           //  program to spawn and arguments
           String prog = "/Users/franck/development/COMP255-2016/assignment-1-solution/bounceInput.sh";
           String[] args = { "2" };

           //  create a process manager to interact with `prog`

           try {
               ProcessManager p = new ProcessManager(prog, args);
               //  spawn and collect response
               p.spawn();
               //    create the prompt
               Pattern prompt = Pattern.compile(">");

               //    send text
               boolean resSend = p.send("packet\n");
               assertEquals(true, resSend);

               //    expect answer in 1sec
               String res = p.expect(prompt , 1);
               assertEquals("packet1", res);
               System.out.println("Test 3 failed");
           }
           catch (Exception e) {
               System.out.println("Test 3 passed");
               throw(e);
           }
       }

      /**
       * Test for spawning a process that outputs a string which is the first arguments passed to it
       */
       @Test
       public void Test4() throws Exception  {

           System.out.println("Testing send(s), s has whitespaces  -- prompt is > -- expect s followed by 1");
           //  program to spawn and arguments
           String prog = "/Users/franck/development/COMP255-2016/assignment-1-solution/bounceInput.sh";
           String[] args = { "0" };

           //  create a process manager to interact with `prog`

           try {
               ProcessManager p = new ProcessManager(prog, args);
               //  spawn and collect response
               p.spawn();

               //    create the prompt
               Pattern prompt = Pattern.compile(">");

               //    send text
               boolean resSend = p.send(" packet \n");
               assertEquals(true, resSend);

               //   expect
               String res = p.expect(prompt , 1);
            //    TimeUnit.SECONDS.sleep(2);
               assertEquals(" packet 1", res);

               System.out.println("Test 4 passed");
           }
           catch (Exception e) {
               System.out.println("Test 4 failed");
               throw(e);
           }
       }
}
