/*
 * This file is part of MQ-ExpectJava.
 *
 * Copyright (C) 2015-2016 Franck Cassez.
 *
 * MQ-ExpectJava is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-ExpectJava is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExpectJava. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

/**
 * Test suite for Longest Common Subsequences
 */
public class ProcessManagerTests {

    /**
     * Test suite examples
     */
    @Test
    public void templateTest() throws IOException {

        //  FIXME
        //  program to spawan and arguments
        String prog = "ps";
        String[] args = { "aux" };

        //  create a process manager to interact with `ps`
        ProcessManager p = new ProcessManager(prog, args);
        //  spawn a process that runs `ps aux` and collect the result
        String res = p.spawnAndCollect();
        assertEquals("something", res);
    }

}
