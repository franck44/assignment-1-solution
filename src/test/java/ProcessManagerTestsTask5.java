/*
 * This file is part of MQ-ExpectJava.
 *
 * Copyright (C) 2015-2016 Franck Cassez.
 *
 * MQ-ExpectJava is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-ExpectJava is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExpectJava. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */
 
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

/**
 * Test suite for Assignment 2 Task 5
 */
public class ProcessManagerTestsTask5 {

    /**
     * Testing multiple send(s) -- process echo with delay of 1s first time and 1s second time
     */
     @Test
     public void Test1() throws Exception  {

         System.out.println("Testing multiple send(s) -- process echo with delay of 1s first time and 1s second time");
         //  program to spawn and arguments
         String prog = "/Users/franck/development/COMP255-2016/assignment-1-solution/bounceInputTwice.sh";
         String[] args = { "1" , "1" };

         //  create a process manager to interact with `prog`

         try {
             ProcessManager p = new ProcessManager(prog, args);
             //  spawn and collect response
             p.spawn();
             //    create the prompt
             Pattern prompt = Pattern.compile(">");

             //    first send
             boolean resSend = p.send("packet\n");
             assertEquals(true, resSend);

             //    expect answer in 2sec
             String res1 = p.expect(prompt , 2000).trim();
             assertEquals("packet1", res1);

             //    second send
             resSend = p.send("packet\n");
             assertEquals(true, resSend);

             //    expect answer in 2sec
             String res2 = p.expect(prompt , 2000).trim();

             assertEquals("packet2", res2);
             System.out.println("Test 1 passed");

         }
         catch (Exception e) {
             System.out.println("Test 1 failed (exception)");
             throw(e);
         }
     }

     /**
      * Testing multiple send(s) -- process echo with delay of 1s first time and 3s second time
      */
      @Test(expected=Exception.class)
      public void Test2() throws Exception  {

          System.out.println("Testing multiple send(s) --process echo with delay of 1s first time and 3s second time");
          //  program to spawn and arguments
          String prog = "/Users/franck/development/COMP255-2016/assignment-1-solution/bounceInputTwice.sh";
          String[] args = { "1" , "3" };

          //  create a process manager to interact with `prog`

          try {
              ProcessManager p = new ProcessManager(prog, args);
              //  spawn and collect response
              p.spawn();
              //    create the prompt
              Pattern prompt = Pattern.compile(">");

              //    first send
              boolean resSend = p.send("packet\n");
              assertEquals(true, resSend);

              //    expect answer in 2sec
              String res1 = p.expect(prompt , 2000).trim();
              assertEquals("packet1", res1);

              //    second send
              resSend = p.send("packet\n");
              assertEquals(true, resSend);

              //    expect answer in 2sec
              String res2 = p.expect(prompt , 2000).trim();
              assertEquals("packet2", res2);
              System.out.println("Test 2 failed");

          }
          catch (Exception e) {
              System.out.println("Test 2 passed (exception)");
              throw(e);
          }
      }
}
