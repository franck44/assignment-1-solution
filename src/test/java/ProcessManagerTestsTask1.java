/*
 * This file is part of MQ-ExpectJava.
 *
 * Copyright (C) 2015-2016 Franck Cassez.
 *
 * MQ-ExpectJava is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-ExpectJava is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExpectJava. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Test suite for Assignment 1 Task 1
 */
public class ProcessManagerTestsTask1 {

    /**
     * Test for spawning a process
     */
    @Test
    public void templateTest() throws Exception  {

        //  FIXME
        //  program to spawan and arguments
        String prog = "/Users/franck/development/COMP255-2016/assignment-1-solution/sleepAndEcho.sh";
        String[] args = { "5" , "20"};

        //  create a process manager to interact with `ps`

        try {
            ProcessManager p = new ProcessManager(prog, args);
            //  spawn a process that runs prog args
            p.spawn();
            TimeUnit.SECONDS.sleep(5);
            p.destroy();
        }
        catch (Exception e) {
            throw(e);
        }
    }

}
