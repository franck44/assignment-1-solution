/*
 * This file is part of MQ-ExpectJava.
 *
 * Copyright (C) 2015-2016 Franck Cassez.
 *
 * MQ-ExpectJava is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-ExpectJava is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExpectJava. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */
 
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunnerTask2 {

    public static void main(String args[]) {

        Result res = JUnitCore.runClasses(ProcessManagerTestsTask2.class);

        if (res.wasSuccessful() != true) {
            System.out.println("Some tests failed: ");
            //  print the failures
            for (Failure f: res.getFailures()) {
                System.out.println("[Test failed] " + f.toString());
            }
        } else {
            System.out.println("All tests passed!");
        }
    }
}
