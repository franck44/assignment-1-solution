/*
 * This file is part of MQ-ExpectJava.
 *
 * Copyright (C) 2015-2016 Franck Cassez.
 *
 * MQ-ExpectJava is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * MQ-ExpectJava is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for  more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExpectJava. (See files COPYING and COPYING.LESSER.)  If
 * not, see  <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.Scanner;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import java.util.regex.Pattern;

/**
 * Create a running process and manage interactions with it
 */
public class ProcessManager {

    /**
     * Program to run and arguments
     */
    String      program;
    String[]    arguments;

    /**
     *  The process that runs the program and
     *  the input/output pipes to this process
     */
    Process                 proc;
    BufferedInputStream     in;
    BufferedOutputStream    out;

    /**
     *  A scanner to read the process output.
     */
    Scanner inputScanner;

    /**
     * Make aa running process with
     *
     *  @param  executable    The program to run
     *  @param  args          The arguments of the program
     */
    public ProcessManager(String executable, String[] args) {
        program = executable;
        arguments = args;
    }

    /**
     * Spawn a process
     */
    public void spawn() throws IOException {

        List<String> commands = new ArrayList<String>();
        commands.add(program);
        for (int i = 0; i < arguments.length; i++ )
        {
            commands.add(arguments[i]);
        }

        // add program and arguments to a list and redirect error to stdout
        ProcessBuilder pb =
            new ProcessBuilder( commands ).redirectErrorStream( true );

        proc = pb.start();

        //  input and output channels of process
        in = new BufferedInputStream( proc.getInputStream() );
        out = new BufferedOutputStream( proc.getOutputStream() );

        //  Scanner to read the inout
        inputScanner = new java.util.Scanner( in );
    }

    /**
     * Spawn a process
     */
    public String spawnAndCollect() throws IOException {
        spawn();
        String res = inputScanner.next();
        return res;
    }

    /**
     * Spawn a process and collect the results or throw an
     * exception if no answer before the timout
     *
     * @param  timeout     The timeout in milliseconds
     *
     * {@link http://winterbe.com/posts/2015/04/07/java8-concurrency-tutorial-thread-executor-examples/}
     */
    public String spawnAndCollectWithTimeout(int timeout) throws
            InterruptedException,
            ExecutionException,
            TimeoutException {

        //  executor service with one thread
        ExecutorService eService = Executors.newFixedThreadPool(1);

        //  create a Future to run the spawnAndCollect method
        Future<String> future = eService.submit( () -> {
            return spawnAndCollect();
        });

        //  collect result or preempt if it takes too long
        try {
            String res =  future.get(timeout, TimeUnit.SECONDS);
            //  shutdown executor service process
            eService.shutdownNow();
            return res;
        } catch (Exception e) {
            throw e;
        } finally {
            //  terminate the executor service in case an excpetion occurs
            eService.shutdownNow();
        }
    }

    /**
     * Send a string to the process
     */
    public boolean send(String s) throws IOException {
        out.write( s.getBytes() );
        out.flush();
        return true;
    }

    /**
    * Collect the result upto a ‘prompt‘ or throw an
    * exception if the prompt is not seen before the timeout
    *
    * @param timeout  The timeout in milliseconds
    * @param prompt   The expected prompt
    */
    public String expect(Pattern prompt, int timeout) throws
            InterruptedException,
            ExecutionException,
            TimeoutException {

        //  executor service with one thread
        ExecutorService eService = Executors.newFixedThreadPool(1);

        //  create a Future to collect until a prompt method
        Future<String> future = eService.submit( () -> {
            String res = inputScanner.useDelimiter( prompt.pattern() ).next();
            return res;
        });

        //  collect result
        try {
            String res =  future.get(timeout, TimeUnit.MILLISECONDS);

            //  The scanner position is on the first character of
            //  the prompt, so we advance it to the end of the
            //  prompt to get ready for the next expect request
            inputScanner.skip( prompt.pattern() );

            //  shutdown executor service process
            eService.shutdownNow();
            return res;
        } catch (Exception e) {
            //  something went wrong kill the process
            proc.destroy();
            throw e;
        } finally {
            //  terminate the executor service in case an excpetion occurs
            eService.shutdownNow();
        }
    }

    /**
     * Kill the process
     */
    public void destroy() {
        //    should kill the running process and its children.
        //    Note that input/output streams may not be closed
        proc.destroyForcibly();
    }
}
