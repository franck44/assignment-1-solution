
This file should provide set up, requirements, and uage examples.

Overview
========

This package provides a simple Java implementation for Expect, more
 specifically the <strong>send</strong> and <strong>expect</strong> commands.
 See the [Expect](http://expect.sourceforge.net)  webpage and
 documentation for details.

 The objective is to programmatically interact with an external process by
 sending data on its input channel (e.g. stdin) and collecting the responses from
 its output channel (e.g. stderr or stdout).  Expect provides an interface to
 achieve this via the *send* and *expect* methods. The
 assumption is that the process' responses are postfixed with a
 *prompt*.

 The prompt can be defined by a simple regular expression. A typical interaction
 with the process is a sequence of *send/expect* commands:
 <ul>
  <li>send a command to the process.</li>
  <li>expect a sequence of characters
  (including spaces/newlines) followed by the prompt.</li>
 </ul>

Examples usage are availabe in the test directory.


Set up
======

You need a recent JDK on your machine.

Recent versions of junit and harmcrest and provided in  src/test/java/lib/

Moreover, the CLASSPATH should be set as follows:
<pre>
> export CLASSPATH="$CLASSPATH:src/test/java/lib/*:src/main/java/:src/test/java/"
</pre>

Compiling
=========
<pre>
> javac src/main/java/*.java src/test/java/*.java
</pre>

Running the tests
=================
<pre>
> java TestRunner
</pre>

The shell script "runall.sh" wil set the classpath, compile and run the tests.
