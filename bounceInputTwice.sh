#!/bin/bash
#

# prevent read from removing leading and trailing white spaces
IFS=

# wait for some input (newline terminated)
read line

# wait $1 seconds before replying
sleep "$1s"

# echoes input followed by digit 1 and the prompt >
echo "${line}1>"

# wait for second input (newline terminated)
read line

# wait $1 seconds before replying
sleep "$2s"

# echoes input followed by digit 2 and the prompt >
echo "${line}2>"

sleep 5s
